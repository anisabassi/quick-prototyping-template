﻿using UnityEngine;

/// <summary>
/// Generic Singleton pattern Class
/// This is a collection of script iteration and modification inspired from many threads from the internet
/// </summary>
/// <typeparam name="T">Generic Class</typeparam>
public class Singleton<T> : MonoBehaviour where T : MonoBehaviour
{
    private static T _instance;

    private static object _lock = new object();

    private static bool _isQuitting = false;

    public static T Instance
    {
        get
        {
            // Prevent mutual-exclusion
            lock (_lock)
            {
                if (_instance == null)
                {
                    _instance = (T)FindObjectOfType(typeof(T));

                    // Prevent double instance of the same type
                    if (FindObjectsOfType(typeof(T)).Length > 1)
                    {
                        Debug.LogWarning("There is More then on instance of " + typeof(T).Name);
                        return _instance;
                    }

                    // Prevent instance to be null or tbe created while the qame is quitting
                    if (_instance == null && !_isQuitting) return null;
                }

                return _instance;
            }
        }
    }

    public static void SetActive(bool _Active)
    {
        Instance.gameObject.SetActive(_Active);
    }

    private void OnDestroy()
    {
        OnDestroySpecific();
    }

    private void OnApplicationQuit()
    {
        _isQuitting = true;
    }

    protected virtual void OnDestroySpecific()
    {
    }
}