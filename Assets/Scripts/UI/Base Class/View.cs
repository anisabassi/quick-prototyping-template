﻿using UnityEngine;

namespace Game
{
    public class View<T> : Singleton<T> where T : MonoBehaviour
    {
        [SerializeField]
        private float fadeInDuration = 0.3f;

        [SerializeField]
        private float fadeOutDuration = 0.3f;

        [SerializeField]
        private CanvasGroup canvasGroup;

        protected bool _visible;

        // Buffers
        private float _startTime;

        private float _duration;
        private bool _inTransition;
        private bool _inOrOut;

        protected virtual void Awake()
        {
            _visible = false;
            canvasGroup.alpha = 0.0f;
            canvasGroup.interactable = false;
            canvasGroup.blocksRaycasts = false;
        }

        public virtual void Show()
        {
            Transition(true);
        }

        public virtual void Hide()
        {
            if (_visible)
                Transition(false);
        }

        private void Transition(bool _InOrOut)
        {
            _visible = _InOrOut;

            _startTime = Time.time;
            _inTransition = true;
            _inOrOut = _InOrOut;
            _duration = _InOrOut ? fadeInDuration : fadeOutDuration;
            canvasGroup.interactable = false;
            canvasGroup.blocksRaycasts = false;
        }

        private void Update()
        {
            if (_inTransition)
            {
                float time = Time.time - _startTime;
                float percent = time / _duration;

                if (percent < 1.0f)
                {
                    canvasGroup.alpha = _inOrOut ? percent : (1.0f - percent);
                }
                else
                {
                    _inTransition = false;
                    canvasGroup.alpha = _inOrOut ? 1.0f : 0.0f;
                    canvasGroup.interactable = _inOrOut;
                    canvasGroup.blocksRaycasts = _inOrOut;
                }
            }
        }
    }
}